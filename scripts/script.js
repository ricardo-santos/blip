$( document ).ready(function() {
 
 	var query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\")";

	var url = "https://query.yahooapis.com/v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&q=";

	function buildRequest(text) {

	    return url + query.replace("%s", text);

	}

	function toCelsius(f) {
    	return parseInt((5/9) * (f-32));
	}

	function setMsgErro(msg){
		
		$("#forecast").empty();

		$("#alert-msg").append(msg);

		$("#alert-msg").removeClass("hidden").delay(3000).queue(function(){
    		$(this).addClass("hidden").dequeue();
    		$(this).last().html("");

		});
		
	}

	function showResults(forecast){

		/*Manipulate dom to show results*/
				
		$("#forecast").empty();

		var list_group,list_group_item,list_group_item_heading,list_group_item_text,strong,texto;
		var forecast_prev = document.getElementById("forecast");		

		for(var i= 0;i<6;i++){
			
			list_group = document.createElement("DIV");
			list_group.className = "list-group";

			list_group_item = document.createElement("A");
			list_group_item.href = "#";
			
			if(i==0)
				list_group_item.className = "list-group-item active";
			else
				list_group_item.className = "list-group-item";


			list_group_item_heading = document.createElement("H4");			
			list_group_item_heading.className = "list-group-item-heading";

			list_group_item_text = document.createElement("P");			
			list_group_item_text.className = "list-group-item-text";

			/*Criar elementos visuais*/		

			strong = document.createElement("STRONG");
			texto = document.createTextNode(forecast[i]['text']+", Max. "+toCelsius(forecast[i]['high']));
			strong.appendChild(texto);
			
			list_group_item_text.appendChild(strong);

			data = document.createTextNode(forecast[i]['day']+", "+forecast[i]['date']);

			list_group_item.appendChild(data);
			list_group_item.appendChild(list_group_item_text);
			list_group.appendChild(list_group_item);

			forecast_prev.appendChild(list_group);
		} 		
		
		$("#forecast").fadeIn();	

			
	}


    $("#search-city").click(function( event ) {
 
 		event.preventDefault();

 		$(this).attr("disabled", true);
 		$(this).button('loading');

 		var city = "";
 		city = $("#city-name").val().toLowerCase();

 		var url = buildRequest(city);

 		$.ajax({
			url: url,			
			error: function() {
			  setMsgErro("Erro na ligação. Tente novamente.");
			  $("#search-city").button('reset');
			},
			dataType: 'json',
			success: function(data) {				

				if(data.query.count == 0 )
					setMsgErro("Cidade não reconhecida.");
				else
					showResults(data.query.results.channel.item.forecast);				

			},
			complete: function(){
				$("#search-city").attr("disabled", false);				
				$("#city-name").val("");

				$("#search-city").button('reset');
			}, 
			type: 'GET'
		});
 
    });
 
});